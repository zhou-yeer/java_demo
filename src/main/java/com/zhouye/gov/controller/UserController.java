package com.zhouye.gov.controller;

import com.zhouye.gov.pojo.User;
import com.zhouye.gov.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/backstage/user")
public class UserController {

    @Autowired
    private UserService userService;

    @PutMapping("/register")
    public void register(User user) throws Exception {
        userService.register(user);
    }
}
