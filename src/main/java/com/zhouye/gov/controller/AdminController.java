package com.zhouye.gov.controller;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zhouye.gov.bean.RoleWithStatus;
import com.zhouye.gov.pojo.Admin;
import com.zhouye.gov.service.AdminService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping("/backstage/admin")
public class AdminController {
    @Autowired
    private AdminService adminService;

    @ApiOperation("分页查询管理员列表")
    @GetMapping("/all")
    @PreAuthorize("hasAnyAuthority('/admin/all')")
    public Page<Admin> all(@RequestParam(defaultValue = "1") int page,
                           @RequestParam(defaultValue = "10") int size){

        Page<Admin> adminPage = adminService.findPage(page, size);
        return adminPage;
    }
    @ApiOperation("新增管理员")
    @PostMapping("/add")
    public void add(Admin admin){
        adminService.add(admin);
    }

    @ApiOperation("根据id查询管理员")
    @GetMapping("/edit")
    public Admin edit(Integer id){
        Admin admin = adminService.findById(id);
        return admin;
    }

    @ApiOperation("修改管理员")
    @PutMapping("/update")
    public void update(Admin admin){
        adminService.update(admin);
    }

    @ApiOperation("根据id查询管理员详情")
    @GetMapping("/desc")
    public Admin desc(Integer aid){
        Admin admin = adminService.findDesc(aid);
        return admin;
    }
    @ApiOperation("根据id查询用户角色情况")
    @GetMapping("/findRole")
    public List<RoleWithStatus> findRole(Integer aid){
        List<RoleWithStatus> roles = adminService.findRole(aid);
        return roles;
    }

    public void mangeRole(Integer aid,Integer[] ids){
        adminService.updateRoles(aid,ids);
    }
    @ApiOperation("更新用户状态")
    @PutMapping("/updateStatus")
    public void updateStatus(Integer aid){
        adminService.updateStatus(aid);
    }

}
