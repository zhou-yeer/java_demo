package com.zhouye.gov.controller;

import com.github.pagehelper.PageInfo;
import com.zhouye.gov.pojo.Article;
import com.zhouye.gov.service.ArticleService;
import com.zhouye.gov.util.JsonResult;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ResponseHeader;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.io.IOException;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/article")
public class ArticleController {

    //注入业务对象
    @Resource
    private ArticleService articleService;

    @ApiOperation("根据搜索查询文章列表")
    @GetMapping("/getlist")
    @ResponseHeader
    public PageInfo<Article> listPage(@RequestParam(defaultValue = "1") Integer pageNum,
                                      @RequestParam(defaultValue = "10") Integer pageSize,
                                      Article article){
        PageInfo<Article> page  = articleService.getList(pageNum , pageSize, article);
        return page;
    }

    @ApiOperation("根据媒体报道类型返回文章列表")
    @PostMapping("/getMtList")
    public JsonResult getSoupList(@RequestParam(defaultValue = "1") Integer pageNum,
                                  @RequestParam(defaultValue = "10") Integer pageSize){
        PageInfo<Article> list = articleService.getSoupList(pageNum,pageSize);
        return JsonResult.ok().data("articleList",list);
    }

    @ApiOperation("根据文章id查询文章所有内容")
    @GetMapping("/getArticle/{id}")
    public JsonResult getArticle(@PathVariable String id){
        Article article = articleService.getById(id);
        return JsonResult.ok().data("article",article);
    }

    @GetMapping("/insertDb")
    public void getAll() throws IOException {
       articleService.insertDb();
    }

    @GetMapping("/getcolumn")
    public Map<String,Integer> getColmn(){
        Map<String,Integer> map = articleService.getColmn();
        return map;
    }

}
