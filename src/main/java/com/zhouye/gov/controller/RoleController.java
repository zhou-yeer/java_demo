package com.zhouye.gov.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zhouye.gov.bean.PermissionWithStatus;
import com.zhouye.gov.pojo.Role;
import com.zhouye.gov.service.RoleService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import java.util.List;


@RestController
@RequestMapping("/backstage/role")
public class RoleController {

    @Autowired
    private RoleService roleService;

    @ApiOperation("分页查询角色列表")
    @RequestMapping("/all")
//    @PreAuthorize("hasAnyAuthority('/role/all')")
    public Page<Role> all(@RequestParam(defaultValue = "1") int page,
                            @RequestParam(defaultValue = "10") int size) {
        Page<Role> rolePage = roleService.findPage(page, size);
        return rolePage;
    }
    @ApiOperation("查询角色的权限情况")
    @GetMapping("/findPermission")
    public List<PermissionWithStatus> findRole(Integer rid) {
        List<PermissionWithStatus> permissions = roleService.findPermission(rid);
        return permissions;
    }
    @ApiOperation("更新角色的权限情况")
    @PutMapping("/updatePermission")
    public void managerPermission(Integer rid,Integer[] ids){
        roleService.updatePermissions(rid, ids);
    }
}