package com.zhouye.gov.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zhouye.gov.pojo.Permission;
import com.zhouye.gov.service.PermissionService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/backstage/permission")
public class PermissionController {
    @Autowired
    private PermissionService permissionService;

    @ApiOperation("分页查询权限列表")
    @GetMapping("/all")
//    @PreAuthorize("hasAnyAuthority('permission/all')")
    public Page<Permission> all(@RequestParam(defaultValue = "1") Integer page,
                            @RequestParam(defaultValue = "10") Integer size){
        Page<Permission> permissionPage = permissionService.findPage(page,size);
        return permissionPage;
    }

    @ApiOperation("新增权限")
    @PostMapping("/add")
    public void add(Permission permission){
        permissionService.add(permission);
    }

    @ApiOperation("根据id查询权限")
    @GetMapping("/edit")
    public Permission edit(Integer pid){
        Permission permission = permissionService.findById(pid);
        return permission;
    }

    @ApiOperation("修改权限")
    @PutMapping("/update")
    public void update(Permission permission){
        permissionService.update(permission);
    }

    @ApiOperation("删除权限")
    @DeleteMapping("/delete")
    public void delete(Integer pid){
        permissionService.delete(pid);
    }
}
