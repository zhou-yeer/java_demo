package com.zhouye.gov.controller;

import com.zhouye.gov.service.PictureService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("picture")
public class   PictureController {

    @Autowired
    private PictureService pictureService;

    @ApiOperation("上传图片")
    @PostMapping("/loadImg")
    public String uploadImg(MultipartFile file){
        String fileName = pictureService.uploadImg(file);
        return fileName;
    }
}
