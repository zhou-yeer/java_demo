package com.zhouye.gov.pojo;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

@Data
public class Permission {
    @TableId
    private Integer pid;
    private String permissionName;  //权限名
    private String permissionDesc;  //权限详情
}
