package com.zhouye.gov.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@TableName("article_mtbd")
@AllArgsConstructor
@NoArgsConstructor
public class Article {

    @TableId(value = "id",type = IdType.ID_WORKER_STR)
    private String  id;     //id
    private String title;  //标题
    private String author; //作者
    private String content;//内容
    private String createDate; //创建日期
    private String url;
}

