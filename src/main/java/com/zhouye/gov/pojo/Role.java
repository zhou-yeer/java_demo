package com.zhouye.gov.pojo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import java.util.List;

@Data
public class Role {
    @TableId
    private Integer rid;
    private String roleName;  //角色名
    private String roleDesc;  //角色介绍
    @TableField(exist = false)
    private List<Permission> permissions;  //权限集合
}
