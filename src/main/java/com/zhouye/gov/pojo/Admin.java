package com.zhouye.gov.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import java.util.List;

@Data
public class Admin {
    @TableId(value = "aid",type = IdType.AUTO)
    private Integer aid;
    private String username;
    private String password;
    private String phoneNum;
    private String email;
    private boolean status;       //状态ture可用,false不可用
    @TableField(exist = false)   //不是数据库字段
    private List<Role> roles;
}
