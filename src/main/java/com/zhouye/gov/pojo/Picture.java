package com.zhouye.gov.pojo;

import lombok.Data;

@Data
public class Picture {
    private int id;

    private String pictureName;

    private String pictureUrl;
}
