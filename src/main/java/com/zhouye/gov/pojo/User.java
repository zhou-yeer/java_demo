package com.zhouye.gov.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

@Data
public class User {
    @TableId(value = "uid",type = IdType.ID_WORKER_STR)
    private String uid;
    private String userName;
    private String userAccount;
    private String userPassword;
    private String userEmail;
}
