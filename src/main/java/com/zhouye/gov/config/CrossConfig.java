package com.zhouye.gov.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;


@Configuration
public class CrossConfig implements WebMvcConfigurer {

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        // 哪种格式的请求路径进行跨域处理
        registry.addMapping("/**")
                // 支持的域
                .allowedOriginPatterns("*")
                // 许的请求方法，默认是 GET、POST 和 HEAD
                .allowedMethods("GET", "HEAD", "POST", "PUT", "DELETE","OPTIONS")
                // 是否发送Cookie信息
                .allowCredentials(true)
                // 探测请求的有效期
                .maxAge(3600)
                // 放行哪些原始域(头部信息)
                .allowedHeaders("*");
    }
}
