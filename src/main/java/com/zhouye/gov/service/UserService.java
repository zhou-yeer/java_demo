package com.zhouye.gov.service;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zhouye.gov.pojo.Article;
import com.zhouye.gov.pojo.User;

public interface UserService extends IService<User> {

    //根据账号查询用户
    Integer selectUser(String userAccount);

    void register(User user) throws Exception;

}
