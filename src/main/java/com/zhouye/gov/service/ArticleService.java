package com.zhouye.gov.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;
import com.zhouye.gov.pojo.Article;
import com.zhouye.gov.util.JsonResult;

import java.io.IOException;
import java.util.List;
import java.util.Map;


public interface ArticleService extends IService<Article> {

    PageInfo<Article> getList(Integer pageNum, Integer pageSize, Article article);

    PageInfo<Article> getSoupList(Integer pageNum, Integer pageSize);

    void insertDb() throws IOException;

    Map<String, Integer> getColmn();
}
