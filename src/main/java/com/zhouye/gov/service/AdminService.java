package com.zhouye.gov.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zhouye.gov.bean.RoleWithStatus;
import com.zhouye.gov.pojo.Admin;
import com.zhouye.gov.pojo.Permission;

import java.util.List;


public interface AdminService {
        //分页查询管理员
        Page<Admin> findPage(int page, int size);

        //新增管理员
        void add(Admin admin);

        //查询管理员
        Admin findById(Integer id);

        //修改管理员
        void update(Admin admin);

        //查询用户详情
        Admin findDesc(Integer aid);

        //查询用户的角色情况
        List<RoleWithStatus> findRole(Integer aid);

        void updateRoles(Integer aid,Integer[] ids);

        //根据名字查询管理员
        Admin findByAdminName(String username);

        //根据名字查询权限
        List<Permission> findAllPermission(String uername);
        //修改用户状态
        void updateStatus(Integer aid);
}
