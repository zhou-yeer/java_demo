package com.zhouye.gov.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zhouye.gov.bean.PermissionWithStatus;
import com.zhouye.gov.pojo.Role;
import java.util.List;

public interface RoleService {

    Page<Role> findPage(int page, int size);

    //查询角色的权限情况
    List<PermissionWithStatus> findPermission(Integer rid);

    //更新角色的所有权限
    void updatePermissions(Integer rid,Integer[] ids);
}
