package com.zhouye.gov.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zhouye.gov.bean.PermissionWithStatus;
import com.zhouye.gov.mapper.PermissionMapper;
import com.zhouye.gov.mapper.RoleMapper;
import com.zhouye.gov.pojo.Permission;
import com.zhouye.gov.pojo.Role;
import com.zhouye.gov.service.RoleService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;


@Service
public class RoleServiceImpl implements RoleService {
    @Autowired
    private RoleMapper roleMapper;

    @Autowired
    private PermissionMapper permissionMapper;

    @Override
    public Page<Role> findPage(int page, int size){
        Page selectPage = roleMapper.selectPage(new Page(page,size),null);
        return selectPage;
    }

    //查询角色的权限情况
    @Override
    public List<PermissionWithStatus> findPermission(Integer rid){
        //查询所有权限
        List<Permission> permissions = permissionMapper.selectList(null);
        //查询角色拥有的所有权限id
        List<Integer> pids = permissionMapper.findPermissionIdByRole(rid);
        //带有状态的权限集合
        List<PermissionWithStatus> permissionList = new ArrayList<>();
        //遍历所有权限
        for(Permission permission :permissions){
            //创建带有状态的权限
            PermissionWithStatus permissionWithStatus = new PermissionWithStatus();
            //将权限属性复制给带有状态的权限
            BeanUtils.copyProperties(permission,permissionWithStatus);
            //如果角色拥有该权限
            if(pids.contains(permission.getPid())){
                permissionWithStatus.setRoleHas(true);
            }else {
                permissionWithStatus.setRoleHas(false);
            }
            permissionList.add(permissionWithStatus);
        }
        return permissionList;
    }
    //更新角色的所有权限
    @Override
    public void updatePermissions(Integer rid,Integer[] ids){
        //删除角色的所有权限
       roleMapper.deleteRoleAllPermission(rid);
       //重新给角色添加权限
       for(Integer pid:ids){
           roleMapper.addRolePermission(rid,pid);
       }
    }
}
