package com.zhouye.gov.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zhouye.gov.mapper.UserMapper;
import com.zhouye.gov.pojo.User;
import com.zhouye.gov.service.UserService;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {


    @Override
    public Integer selectUser(String userAccount) {
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper.eq("userAccount",userAccount);
        return baseMapper.selectCount(wrapper);
    }

    @Override
    public void register(User user) throws Exception {
        Integer integer = selectUser(user.getUserAccount());
        if(integer == 0){
            baseMapper.insert(user);
        }else{
            throw new Exception("用户已存在");
        }
    }
}
