package com.zhouye.gov.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zhouye.gov.mapper.PermissionMapper;
import com.zhouye.gov.pojo.Permission;
import com.zhouye.gov.service.PermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PermissionServiceImpl implements PermissionService {

    @Autowired
    private PermissionMapper permissionMapper;

    public Page<Permission> findPage(int page, int size){
        Page selectPage = permissionMapper.selectPage(new Page(page,size),null);
        return selectPage;
    }
    //新增权限
    @Override
    public void add(Permission permission) {
        permissionMapper.insert(permission);
    }

    //修改权限
    @Override
    public Permission findById(Integer pid){
        return permissionMapper.selectById(pid);
    }
    @Override
    public void update(Permission permission){
        permissionMapper.updateById(permission);
    }

    //删除权限
    @Override
    public void delete(Integer pid){
        permissionMapper.deleteById(pid);
    }
}
