package com.zhouye.gov.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.zhouye.gov.mapper.ArticleMapper;
import com.zhouye.gov.pojo.Article;

import com.zhouye.gov.service.ArticleService;
import com.zhouye.gov.util.JsonResult;
import com.zhouye.gov.util.JsoupArticleListUtil;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service
public class ArticleServiceImpl extends ServiceImpl<ArticleMapper,Article> implements ArticleService {

    //注入Mapper接口对象
    @Resource
    private ArticleMapper articleMapper;
    @Resource
    private JsoupArticleListUtil jsoupArticleListUtil;
    @Override
    public PageInfo<Article> getList(Integer pageNum, Integer pageSize, Article article) {
        //设置每页的记录数
        PageHelper.startPage(pageNum, pageSize);
        List<Article> list = articleMapper.getList(article);
        PageInfo page = new PageInfo(list);
        return page;
    }


    @Override
    public PageInfo<Article> getSoupList(Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<Article> list = articleMapper.getSoupList();
        PageInfo page = new PageInfo(list);
        return page;
    }

    @Override
    public void insertDb() throws IOException {
        List<Article> infoList = jsoupArticleListUtil.getArticleList();
        for(Article article:infoList){
            String link = article.getUrl();
            String url = "https://www.jsdj.gov.cn"+link.substring(0,link.indexOf("?"));
            Article articleAll = jsoupArticleListUtil.getArticleAll(url);
            articleAll.setTitle(article.getTitle());
            articleAll.setCreateDate(article.getCreateDate());
            articleAll.setUrl(url);
            QueryWrapper<Article> wrapper = new QueryWrapper<>();
            wrapper.eq("title",article.getTitle());
            Integer integer = baseMapper.selectCount(wrapper);
            if(integer == 0){
                baseMapper.insert(articleAll);
            }
        }
    }

    @Override
    public Map<String,Integer> getColmn() {
        Integer mtbdColmn = articleMapper.getMtbdColmn();
        Integer workColmn = articleMapper.getWorkColmn();
        Integer aroundColmn = articleMapper.getAroundColmn();
        Integer buildColmn = articleMapper.getBuildColmn();
        Integer classColmn = articleMapper.getClassColmn();
        Integer policyColmn = articleMapper.getPolicyColmn();

        Map<String,Integer> column = new HashMap<>();
        column.put("mtbdColmn",mtbdColmn);
        column.put("workColmn",workColmn);
        column.put("aroundColmn",aroundColmn);
        column.put("buildColmn",buildColmn);
        column.put("classColmn",classColmn);
        column.put("policyColmn",policyColmn);

        return column;
    }
}
