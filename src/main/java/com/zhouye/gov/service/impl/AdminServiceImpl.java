package com.zhouye.gov.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zhouye.gov.bean.RoleWithStatus;
import com.zhouye.gov.mapper.AdminMapper;
import com.zhouye.gov.mapper.RoleMapper;
import com.zhouye.gov.pojo.Admin;
import com.zhouye.gov.pojo.Permission;
import com.zhouye.gov.pojo.Role;
import com.zhouye.gov.service.AdminService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;


@Service
public class AdminServiceImpl implements AdminService {
    @Autowired
    private AdminMapper adminMapper;
    @Autowired
    private RoleMapper roleMapper;
    @Autowired
    private NoOpPasswordEncoder encoder;

    //分页查询管理员
    @Override
    public Page<Admin> findPage(int page, int size) {
        Page selectPage = adminMapper.selectPage(new Page(page,size),null);
        return selectPage;
    }

    //添加管理员
    @Override
    public void add(Admin admin) {
        admin.setPassword(encoder.encode(admin.getPassword()));
        adminMapper.insert(admin);
    }
    //根据id查询管理员
    @Override
    public Admin findById(Integer id) {
        return adminMapper.selectById(id);
    }
    //更新管理员密码
    @Override
    public void update(Admin admin) {
        //旧密码
        String oldPassword = adminMapper.selectById(admin.getAid()).getPassword();
        //新密码
        String newPassword = admin.getPassword();
        if(!oldPassword.equals(newPassword)){
            admin.setPassword(encoder.encode(newPassword));
        }
        adminMapper.updateById(admin);
    }

    //查询管理员详细信息
    @Override
    public Admin findDesc(Integer aid) {
        return adminMapper.findDesc(aid);
    }

    //查询用户的角色情况
    @Override
    public List<RoleWithStatus> findRole(Integer aid) {
        //查询所有角色
        List<Role> roles = roleMapper.selectList(null);
        //查询用户拥有的所有角色id
        List<Integer> rids = roleMapper.findRoleIdByAdmin(aid);
        //带有状态的角色集合
        List<RoleWithStatus> roleList = new ArrayList<>();
        //遍历所有角色
        for(Role role:roles){
            RoleWithStatus roleWithStatus = new RoleWithStatus();
            //将角色属性复制给带有状态的角色
            BeanUtils.copyProperties(role,roleWithStatus);
            if(rids.contains(role.getRid())){  //用户拥有该角色  角色状态变为true
                roleWithStatus.setAdminHas(true);
            }else {
                roleWithStatus.setAdminHas(false);
            }
            roleList.add(roleWithStatus);
        }
        return roleList;
    }

    //更新用户的角色
    @Override
    public void updateRoles(Integer aid, Integer[] ids) {
        //删除用户的所有角色
        adminMapper.deleteAdminAllRoles(aid);
        //重新给用户添加角色
        for(Integer rid:ids){
            adminMapper.addAdminRole(aid,rid);
        }
    }

    //根据名字查询管理员
    @Override
    public Admin findByAdminName(String username) {
        QueryWrapper<Admin> wrapper = new QueryWrapper<>();
        wrapper.eq("username",username);
        Admin admin = adminMapper.selectOne(wrapper);
        return admin;
    }
    //根据名字查询权限
    @Override
    public List<Permission> findAllPermission(String uername) {
        return adminMapper.findAllPermission(uername);
    }

    //修改用户的状态
    @Override
    public void updateStatus(Integer aid){
        Admin admin = adminMapper.selectById(aid);
        admin.setStatus(!admin.isStatus());
        adminMapper.updateById(admin);
    }
}
