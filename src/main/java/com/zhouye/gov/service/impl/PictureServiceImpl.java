package com.zhouye.gov.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zhouye.gov.mapper.PictureMapper;
import com.zhouye.gov.pojo.Picture;
import com.zhouye.gov.service.PictureService;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import java.io.File;
import java.io.IOException;
import java.util.UUID;

@Service
public class PictureServiceImpl extends ServiceImpl<PictureMapper, Picture> implements PictureService {

    @Override
    public String uploadImg(MultipartFile file) {

        String basePath = "var/local/picture/";

        //获取原始文件名
        String originalFilename = file.getOriginalFilename();
        //获取后缀名
        String suffix = originalFilename.substring(originalFilename.lastIndexOf("."));
        //使用UUID重新生成文件名，防止文件名重复造成文件覆盖
        String fileName = UUID.randomUUID().toString() + suffix;

        //创建一个目录对象
        File dir = new File(basePath);
        //判断当前目录是否存在
        if (!dir.exists()) {
            //目录不存在，需要创建
            dir.mkdirs();
        }

        String url = basePath+fileName;

        //保存文件
        try {
            file.transferTo(new File(url));
            Picture picture = new Picture();
            picture.setPictureName(fileName);
            picture.setPictureUrl(url);
            baseMapper.insert(picture);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return fileName;
    }
}
