package com.zhouye.gov.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zhouye.gov.pojo.Picture;
import org.springframework.web.multipart.MultipartFile;

public interface PictureService extends IService<Picture> {

    /**
     * 上传图片
     * @param file
     * @return
     */
    String uploadImg(MultipartFile file);
}
