package com.zhouye.gov.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zhouye.gov.pojo.Permission;

public interface PermissionService {

     Page<Permission> findPage(int page, int size);
    //新增权限
     void add(Permission permission);

    //修改权限
     Permission findById(Integer pid);

     void update(Permission permission);

    //删除权限
     void delete(Integer pid);
}
