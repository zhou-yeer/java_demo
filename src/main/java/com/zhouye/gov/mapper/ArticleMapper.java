package com.zhouye.gov.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zhouye.gov.pojo.Article;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import java.util.List;
import java.util.Map;

@Repository
@Mapper
public interface ArticleMapper extends BaseMapper<Article> {

    List<Article> getList(Article article);

    List<Article> getSoupList();

    Integer  getMtbdColmn();

    Integer  getWorkColmn();

    Integer  getClassColmn();

    Integer  getBuildColmn();

    Integer  getPolicyColmn();

    Integer  getAroundColmn();

}
