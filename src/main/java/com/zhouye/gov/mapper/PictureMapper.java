package com.zhouye.gov.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zhouye.gov.pojo.Picture;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Repository
@Mapper
public interface PictureMapper extends BaseMapper<Picture> {
}
