package com.zhouye.gov.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zhouye.gov.pojo.User;

public interface UserMapper extends BaseMapper<User> {

}
