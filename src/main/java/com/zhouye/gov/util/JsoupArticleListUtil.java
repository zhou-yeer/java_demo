package com.zhouye.gov.util;

import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.zhouye.gov.pojo.Article;
import jdk.nashorn.internal.objects.NativeUint16Array;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Component;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

@Component
public class JsoupArticleListUtil {
    public String link;
    /**
     * 使用htmlunit来模拟浏览器，并且等待js加载完毕后，再读取整个页面
     * @param url
     * @return
     * @throws IOException
     */
    public String getPageWaitJS (String url) throws IOException {
        WebClient webClient = new WebClient();
        webClient.getOptions().setJavaScriptEnabled(true); //启用JS解释器，默认为true
        webClient.getOptions().setCssEnabled(false); //禁用css支持
        webClient.getOptions().setThrowExceptionOnScriptError(false); //js运行错误时，是否抛出异常
        HtmlPage page = webClient.getPage(url);
        webClient.waitForBackgroundJavaScript(3*1000);
        String pageXml = page.asXml(); //以xml的形式获取响应文本
        return pageXml;
    }

    public List<Article> getArticleList() {
        // 用List存放爬取的文章信息
        List<Article> articleList = new ArrayList<>();
        for(int i = 0;i < 10;i++){
            String url = "https://www.jsdj.gov.cn/category/szzl.html#"+ i;


        // html的Document对象
        Document doc = null;
            try {
                String pageWaitJS = getPageWaitJS(url);
                doc = Jsoup.parse(pageWaitJS);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if(doc == null){
                break;
            }
            for (int j = 0; j < doc.select("#ntrJbtt > li").size(); j++) {
                if (null != doc) {
                    Element articleLi = doc.select("#ntrJbtt > li").get(j);
                    if (null != articleLi) {
                        String date = articleLi.selectFirst("span[class=j-date]").text();
                        link = articleLi.select("a").first().attr("href");
                        String title = articleLi.select("a").first().attr("title");
                        if (date != null && link != null && title != null) {
                            Article article = new Article();
                            article.setCreateDate(date);
                            article.setUrl(link);
                            article.setTitle(title);
                            articleList.add(article);
                        }
                    }
                }
            }
        }
        return articleList;
    }
    public Article getArticleAll(String url) throws IOException {
        //解析网页，30s内未爬取成功，打印错误
        Document document = Jsoup.parse(new URL(url),30000);
        //获取文章的来源
        String author1 = document.getElementsByClass("j-article-top").first().child(0).text();
        String author2 = document.getElementsByClass("j-article-top").first().child(1).text();
        //获取文章的内容
        Elements content = document.select(".j-article-center > p");
        Article article = new Article();
        article.setAuthor(author1+" "+author2);
        article.setContent(content.toString());
        return article;
    }
}
