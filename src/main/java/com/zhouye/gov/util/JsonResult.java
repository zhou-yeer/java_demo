package com.zhouye.gov.util;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.HashMap;
import java.util.Map;

/**
 * @Description: 统一返回结果
 * @author: zhangyulin
 * @date:2022/8/13 13:12
 */
@Data
public class JsonResult {

    @ApiModelProperty(value = "是否成功")
    private Boolean success;

    @ApiModelProperty(value = "返回码")
    private Integer code;

    @ApiModelProperty(value = "返回消息")
    private String message;

    @ApiModelProperty(value = "返回数据")
    private Map<String, Object> data = new HashMap<String, Object>();

    //把构造方法私有
    private JsonResult() {}

    //成功静态方法
    public static JsonResult ok() {
        JsonResult jsonResult = new JsonResult();
        jsonResult.setSuccess(true);
        jsonResult.setCode(ResultCode.SUCCESS);
        jsonResult.setMessage("成功");
        return jsonResult;
    }

    //失败静态方法
    public static JsonResult error() {
        JsonResult jsonResult = new JsonResult();
        jsonResult.setSuccess(false);
        jsonResult.setCode(ResultCode.ERROR);
        jsonResult.setMessage("失败");
        return jsonResult;
    }

    public JsonResult success(Boolean success){
        this.setSuccess(success);
        return this;
    }

    public JsonResult message(String message){
        this.setMessage(message);
        return this;
    }

    public JsonResult code(Integer code){
        this.setCode(code);
        return this;
    }

    public JsonResult data(String key, Object value){
        this.data.put(key, value);
        return this;
    }

    public JsonResult data(Map<String, Object> map){
        this.setData(map);
        return this;
    }

}
