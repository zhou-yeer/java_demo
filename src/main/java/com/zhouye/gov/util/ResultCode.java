package com.zhouye.gov.util;

/**
 * @Description:
 * @author: zhangyulin
 * @date:2022/8/13 13:08
 */
public interface ResultCode {

    Integer SUCCESS = 20000;
    Integer ERROR = 20001;

}
